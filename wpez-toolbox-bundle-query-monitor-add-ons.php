<?php
/*
Plugin Name: WPezToolbox - Bundle: Query Monitor Add-Ons
Plugin URI: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-query-monitor-add-ons
Description: QM+ - A WPezToolbox bundle of third-party add-ons for the ever-popular Query Monitor plugin.
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleQueryMonitorAddOns;

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__ . '\plugins' );


function plugins( $arr_in = [] ) {

	$str_bundle           = 'QM+';
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [


		// ===== QM3 - Third-party plugins that extend Query Monitor =====
		// https://github.com/johnbillion/query-monitor/wiki/Query-Monitor-Add-on-Plugin


		// Query Monitor Extend - Lists conditionals for WooCommerce, plus many core constants and paths.
		// https://github.com/crstauf/query-monitor-extend

		'query-monitor-extend'                           =>
			[
				'name'     => $str_prefix . 'Query Monitor: Extend',
				'slug'     => 'query-monitor-extend',
				'source'   => 'https://github.com/crstauf/query-monitor-extend/archive/master.zip',
				'required' => false,
				'wpez'     => [

					'info'      => [
						'by'     => 'Caleb Stauffer',
						'url_by' => 'http://develop.calebstauffer.com/',
						'desc'   => 'WordPress plugin with customizations to enhance/extend the already awesome Query Monitor plugin.',
						'url_img'    => 'https://avatars1.githubusercontent.com/u/4573033?s=460&v=4',
					],
					'resources' => [
						'url_repo' => 'https://github.com/crstauf/query-monitor-extend',
						'url_fb'   => 'https://www.facebook.com/calebstaufferstyle',
					]
				],
			],

		// Query Monitor: Included Files - Shows the included files for each page load, along with their component and file size.
		// https://github.com/khromov/wp-query-monitor-included-files
		'wp-query-monitor-included-files'                =>
			[
				'name'     => $str_prefix . 'Query Monitor: Included Files',
				'slug'     => 'wp-query-monitor-included-files',
				'source'   => 'https://github.com/khromov/wp-query-monitor-included-files/archive/master.zip',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'     => 'Stanislav Khromov',
						'url_by' => 'https://khromov.se/',
						'desc'   => 'Shows number of included files in admin bar and in the footer of each page load. Requires Query Monitor plugin.',
						'url_img'    => 'https://avatars0.githubusercontent.com/u/1207507?s=460&v=4',
					],
					'resources' => [
						'url_repo' => 'https://github.com/khromov/wp-query-monitor-included-files',
						'url_tw'   => 'https://twitter.com/khromov',
					]
				],
			],

		// Query Monitor: Sage Template: Included Files - Shows the active Sage template in use.
		// https://github.com/khromov/wp-query-monitor-sage-template
		'wp-query-monitor-sage-template'                 =>
			[
				'name'     => $str_prefix . 'Query Monitor: Sage Template',
				// The plugin name.
				'slug'     => 'wp-query-monitor-sage-template',
				// The plugin slug (typically the folder name).
				'source'   => 'https://github.com/khromov/wp-query-monitor-sage-template/archive/master.zip',
				// The plugin source.
				'required' => false,
				// If false, the plugin is only 'recommended' instead of required.
				//  'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
				'wpez'     => [
					'info'      => [
						'by'     => 'Stanislav Khromov',
						'url_by' => 'https://khromov.se/',
						'desc'   => 'Shows the active Sage template in us',
						'url_img'    => 'https://avatars0.githubusercontent.com/u/1207507?s=460&v=4',
					],
					'resources' => [
						'url_repo' => 'https://github.com/khromov/wp-query-monitor-sage-template/',
						'url_tw'   => 'https://twitter.com/khromov',
					]
				],
			],

		// Query Monitor: bbPress & BuddyPress Conditionals - Adds bbPress & BuddyPress functions to the Conditionals panel.
		// https://wordpress.org/plugins/query-monitor-bbpress-buddypress-conditionals/
		'query-monitor-bbpress-buddypress-conditionals' =>
			[
				'name'      => $str_prefix . 'Query Monitor: bbPress & BuddyPress Conditionals',
				'slug'      => 'query-monitor-bbpress-buddypress-conditionals',
				'required'  => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Stephen Edgar',
						'url_by'  => 'https://netweb.com.au/',
						'desc'    => 'Add bbPress and BuddyPress Conditionals to John Blackbourn\'s awesome Query Monitor',
						'url_img' => 'https://avatars2.githubusercontent.com/u/1016458?s=460&v=4',
					],
					'resources' => [
						'url_wp_org'  => 'https://wordpress.org/plugins/query-monitor-bbpress-buddypress-conditionals/',
						'url_repo'    => 'https://github.com/ntwb/query-monitor-bbs-conds',
						'url_tw'      => 'https://twitter.com/netweb',
					],
				],
			],

		'query-monitor-extension-checking-variables'    =>
			[
				'name'     => $str_prefix . 'Query Monitor: Checking Variable',
				'slug'     => 'query-monitor-extension-checking-variables',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Sujin Choi',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/sujin2f/',
						'desc'    => 'Check the value of variables in your code. Outputs to Query Monitor or to the browser console.',
						'url_img' => 'https://ps.w.org/query-monitor-extension-checking-variables/assets/icon-128x128.png?rev=1143565',
					],
					'resources' => [
						'url_wp_org'  => 'https://wordpress.org/plugins/query-monitor-extension-checking-variables/',
					],
				],
			],

		// Query Monitor: Memcache Stats  - Shows Memcache stats in Query Monitor.
		//
		'qm-memcache-stats'                              =>
			[
				'name'     => $str_prefix . 'Query Monitor: Memcache Stats',
				'slug'     => 'qm-memcache-stats',
				'source'   => 'https://github.com/Automattic/qm-memcache-stats/archive/master.zip',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Automattic',
						'url_by'  => 'https://automattic.com/',
						'desc'    => 'WordPress plugin to provide Memcache Stats from to the awesome Query Monitor plugin.',
						'url_img' => 'https://avatars2.githubusercontent.com/u/887802?s=200&v=4',
					],
					'resources' => [
						'url_repo'    => 'https://github.com/Automattic/qm-memcache-stats',
						'url_fb'      => 'http://www.facebook.com/AutomatticInc',
						'url_tw'      => 'https://twitter.com/automattic',
					],
				],

			],

		//
		//
		'querymonitor-givewp'                            =>
			[
				'name'     => $str_prefix . 'Query Monitor: GiveWP Conditionals',
				'slug'     => 'QueryMonitor-GiveWP',
				'source'   => 'https://github.com/tw2113/QueryMonitor-GiveWP/archive/master.zip',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Michael Beckwith',
						'url_by'  => 'https://apiratelifefor.me/',
						'desc'    => 'Query Monitor: GiveWP Conditionals  - Add Give conditionals to Query Monitor.',
						'url_img' => 'https://avatars3.githubusercontent.com/u/484075?s=460&v=4',
					],
					'resources' => [
						'url_repo'    => 'https://github.com/tw2113/QueryMonitor-GiveWP',
						'url_tw'      => 'https://twitter.com/tw2113',
					],
				],
			],

		//
		'query-monitor-flamegraph'                       =>
			[
				'name'     => $str_prefix . 'Query Monitor: Flamegraph',
				'slug'     => 'query-monitor-flamegraph',
				'source'   => 'https://github.com/humanmade/query-monitor-flamegraph/archive/master.zip',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Human Made',
						'url_by'  => 'https://humanmade.com/',
						'desc'    => 'This Query Monitor extension will add profiling framegraphs to Query Monitor via the xhprof PHP extension.',
						'url_img' => 'https://avatars0.githubusercontent.com/u/644666?s=200&v=4',
					],
					'resources' => [
						'url_repo'    => 'https://github.com/humanmade/query-monitor-flamegraph',
						'url_fb'      => 'https://www.facebook.com/humanmadeltd/',
						'url_tw'      => 'https://twitter.com/humanmadeltd',
					],
				],
			],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}